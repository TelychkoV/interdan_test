import React, { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useSelector } from '../redux/types';
import SelectEquipment from '../components/SelectEqiupment';
import SelectColor from '../components/SelectColor';
import { checkCarRequest } from '../redux/middlewares/checkCar.request';
import '../styles/Settings.scss';

const Settings: FC = () => { 
    const dispatch = useDispatch();
    const { option, model, color } = useSelector(state => state.modelReducer);
    const [ step, setStep ] = useState(1);  
    const history = useHistory();
    
    const goToModels = () => history.push('/models');

    const goToColorSettings = () => {
        setStep(2);
    }

    const goToTrimSettings = () => {
        setStep(1);
    }

    const proceedOperation = () => {
        const data = {
            modelName: model!.name,
            trimName: model!.trims[option].name,
            colorName: model!.trims[option].colors[color].name
        }
        dispatch(checkCarRequest(data));
    };
    
    return (
        <section className='settings'>
            {step === 1 ? <SelectEquipment /> : <SelectColor />}
            <article className='settings-navigation'>
                <span 
                    onClick={step === 1 ? goToModels : goToTrimSettings}
                    className='settings-navigation--left'
                >&#8592;
                </span>
                {step === 1 ?
                    <span className='wrapper' onClick={goToColorSettings}>
                        <span className='settings-navigation--right'>&#8594;</span> 
                    </span> : 
                    <button 
                        onClick={proceedOperation} 
                        className='settings-navigation--proceed'>Proceed
                    </button> 
                }
            </article>
        </section>
    )
}

export default Settings; 