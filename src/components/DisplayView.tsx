import React, { FC } from 'react';
import { computeValue } from '../shared/helpers';
import { useSelector } from '../redux/types';
import '../styles/DisplayView.scss';

const DisplayView: FC = () => {   
    const { option, color, model, price } = useSelector(state => state.modelReducer);
    
    return (
        <section className='viewDetails'>
            {model && model!.trims ? 
                <>
                    <h1 className='viewDetails-title'>{model.name}</h1>
                    <p>{model!.trims[option].name}</p>
                    <img 
                        src={model!.trims[option].colors[color].imageUrl} 
                        alt={model!.trims[option].name} 
                        className='viewDetails-image' 
                    />
                    <h2 className='viewDetails-price'>{computeValue(price)} kr.</h2> 
                </> : null
            }
        </section>
    )
}

export default DisplayView;