import React, { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from '../redux/types';
import { IColor } from '../redux/types';
import { computeValue } from '../shared/helpers';
import { selectColorAction } from '../redux/actions/getModel.actions';
import '../styles/SelectColor.scss';

const SelectColor: FC = () => { 
    const dispatch = useDispatch();
    const { option, model, color } = useSelector(state => state.modelReducer);

    const selectColor = (index: number, price: number) => {
        dispatch(selectColorAction(index, price));
    }

    // console.log(color);
    
    return (
        <>
            <h1>SELECT COLOR</h1>
            <section className='colors'>
                {model && model!.trims && 
                 model!.trims[option].colors.map((_color: IColor, index: number) => (
                    <article 
                        key={index}  
                        className='colors-details'
                        onClick={() => selectColor(index, _color.price)}>
                            <img 
                                src={_color.iconUrl} 
                                alt={_color.name} 
                                title={_color.name} 
                                className={`colors-details--image${color === index ? '-active' : ''}`} />
                            <p className='colors-details--name'>{_color.name}</p>
                            <span className='colors-details--additionalPrice'>
                                +{computeValue(_color.price)} kr.
                            </span>
                    </article>
                ))}
            </section>
        </> 
            
    )
}

export default SelectColor;