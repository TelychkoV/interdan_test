import React, { FC } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { UseRoutes } from '../shared/Routes';
import '../styles/App.scss';

const App: FC = () => (
  <Router>
    <main>
      <UseRoutes />
    </main>
  </Router> 
);

export default App;
