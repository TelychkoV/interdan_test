import React, { FC } from 'react';
import { IModel } from '../redux/types';
import { computeValue } from '../shared/helpers';
import '../styles/Model.scss';

interface IProps {
    model: IModel
}

const Model: FC<IProps> = ({ model }: IProps) => (
    <figure className='modelDescription'>
        <img src={model.imageUrl} alt={model.name} className='modelDescription-image' />
        <figcaption>
            <h4 className='modelDescription-name'>{model.name}</h4>
            <h2 className='modelDescription-price'>{computeValue(+model.priceFrom)} kr.</h2>
        </figcaption>
    </figure>
)

export default Model;