import React, { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from '../redux/types';
import { ITrim } from '../redux/types';
import { computeValue } from '../shared/helpers';
import { chooseOptionAction } from '../redux/actions/getModel.actions';
import '../styles/SelectEquipment.scss';

const SelectEquipment: FC = () => { 
    const dispatch = useDispatch();
    const { option, model } = useSelector(state => state.modelReducer);

    const chooseOption = (index: number) => {
        dispatch(chooseOptionAction(index))
    }

    return (
        <>
            <h1>CHOOSE EQUIPMENT LEVEL</h1>
            {model && model!.trims.map((trim: ITrim, index: number) => (
                <div 
                    key={trim.name} 
                    className={option === index ? 'active' : 'inactive'} 
                    onClick={() => chooseOption(index)}>
                        <h4 style={{ color: option === index ? '#ffffff' : '#007edb' }}>
                            {trim.name.toUpperCase()}
                        </h4>
                        <p>{computeValue(+trim.price)} kr.</p>
                </div>
            ))}
        </> 
            
    )
}

export default SelectEquipment;