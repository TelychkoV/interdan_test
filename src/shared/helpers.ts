import { IColorOrTrim } from '../redux/types';

export const computeValue = (value: number) => (value / 1000).toFixed(3);
export const reusableSort = (values: any) => values.sort((a: IColorOrTrim, b: IColorOrTrim): number => a.price - b.price);