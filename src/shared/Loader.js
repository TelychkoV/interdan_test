import React from 'react';
import { css } from "@emotion/core";
import BeatLoader from "react-spinners/BeatLoader";

const override = css`
  margin: 22% auto 0 auto
`;

const Loader = () => (
    <BeatLoader 
        css={override}
        size={15}
        color='black'
    />
)

export default Loader;