import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Models from "../pages/Models";
import ModelConfigurator from '../pages/ModelConfigurator';
import Checkout from '../pages/Checkout';
import SelectEquipment from '../components/SelectEqiupment';
import SelectColor from '../components/SelectColor';

const routes = [
    { 
        path: '/models', 
        component: Models,
    },
    {
        path: '/models/:code/:entity',
        component: ModelConfigurator,
        routes: [
            {
                path: '/trims',
                component: SelectEquipment,
            },
            {
                path: '/colors',
                component: SelectColor,
            },
        ]
    },
    { 
        path: '/checkout/:id', 
        component: Checkout
    }
];

export const UseRoutes = () => (
    <>
        {routes.map(route => (
            <Route key={route.path} path={route.path} exact render={(props) => (
                <route.component {...props} routes={route.routes} />
            )} /> 
        ))}
        <Redirect to='/models' />
    </>
)