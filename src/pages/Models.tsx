import React, { FC, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from "react-redux";
import { getModelsRequest } from '../redux/middlewares/getModels.request';
import { useSelector, IModel } from '../redux/types';
import Model from '../components/Model';
import Loader from '../shared/Loader';
import '../styles/Models.scss';

const Models: FC = () => {
    const dispatch = useDispatch();
    const { loading, models, error } = useSelector(state => state.modelsReducer);
    
    useEffect(() => {
        dispatch(getModelsRequest());
    }, [dispatch])

    if(loading) {
        return <Loader />;
    }

    if(error) {
        return <p>Error</p>;
    }

    return (
        <>
         <h1 className='modelsTitle'>CHOOSE YOUR NEW CAR</h1>
         <section className='modelsList'>
            {models?.map((model: IModel) => ( 
                <Link
                    to={{ pathname: `models/${model.code}/trims`, state: model.code }} 
                    className='modelsList-block'
                    key={model.code}>
                    <Model model={model} />
                </Link>  
            )
            )}
         </section>
        </>
       
    )
}

export default Models;