import React, { FC, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from "react-redux";
import { getModelRequest } from '../redux/middlewares/getModel.request';
import { useSelector } from '../redux/types';
import Loader from '../shared/Loader';
import Settings from '../components/Settings';
import DisplayView from '../components/DisplayView';
import '../styles/ModelConfigurator.scss';

const ModelConfigurator: FC = () => {
    const dispatch = useDispatch();
    const { loading, status, error } = useSelector(state => state.modelReducer);
    const location = useLocation();
    const history = useHistory();

    useEffect(() => {
        dispatch(getModelRequest(location.state));
    }, [location.state, dispatch]);

    useEffect(() => {
        if(typeof status === 'boolean') {
            history.push(`/checkout/${status ? 'success': 'failure'}`, { status });
        }
    }, [status, history]);

    if(loading) {
        return <Loader />;
    }

    if(error) {
        return <p>Error</p>;
    }
    
    return (
        <div className='config'>
            <DisplayView />
            <Settings />
        </div>
    )
}

export default ModelConfigurator;