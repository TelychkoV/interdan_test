import React, { FC } from 'react';
import '../styles/Checkout.scss';

const Checkout: FC = (props: any) => (
        <div className='checkout'>
        {!props.location.state.status ?
            <svg width="160px" height="160px" viewBox="0 0 160 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>status 2</title>
                <desc>Created with Sketch.</desc>
                <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="confirmation-page_status-2" transform="translate(-560.000000, -260.000000)">
                        <g id="content" transform="translate(560.000000, 260.000000)">
                            <g id="status-2">
                                <path d="M80,0 C124.18278,-8.11624501e-15 160,35.81722 160,80 C160,124.18278 124.18278,160 80,160 C35.81722,160 5.41083001e-15,124.18278 0,80 C-5.41083001e-15,35.81722 35.81722,8.11624501e-15 80,0 Z" id="background" fill="#FF0000"></path>
                                <path d="M97.8564213,58 L102.122884,62.266463 L84.327,80.061 L102.122884,97.8564213 L97.8564213,102.122884 L80.061,84.327 L62.266463,102.122884 L58,97.8564213 L75.795,80.061 L58,62.266463 L62.266463,58 L80.061,75.795 L97.8564213,58 Z" id="icon-status-2" fill="#FFFFFF" transform="translate(80.061442, 80.061442) scale(-1, 1) rotate(-90.000000) translate(-80.061442, -80.061442) "></path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg> :
            <svg width="160px" height="160px" viewBox="0 0 160 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <title>status 1</title>
                <desc>Created with Sketch.</desc>
                <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="confirmation-page_status-1" transform="translate(-560.000000, -260.000000)">
                        <g id="content" transform="translate(560.000000, 260.000000)">
                            <g id="status-1">
                                <path d="M80,0 C124.18278,-8.11624501e-15 160,35.81722 160,80 C160,124.18278 124.18278,160 80,160 C35.81722,160 5.41083001e-15,124.18278 0,80 C-5.41083001e-15,35.81722 35.81722,8.11624501e-15 80,0 Z" id="background" fill="#5EE043"></path>
                                <path d="M103.794979,46.2853918 L108.061442,50.5518548 L72.5415578,86.0703918 L93.9193065,107.448145 L89.6528436,111.714608 L68.2755578,90.3363918 L68.2050208,90.4082761 L63.9385578,86.1418131 L64.0095578,86.0703918 L63.9385578,86.0003225 L68.2050208,81.7338595 L68.2755578,81.8043918 L103.794979,46.2853918 Z" id="icon-status-1" fill="#FFFFFF" transform="translate(86.000000, 79.000000) scale(-1, 1) rotate(-90.000000) translate(-86.000000, -79.000000) "></path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        }
    </div>
)

export default Checkout;