import { ICheckCarData } from '../redux/types';

interface IParams {
    method: string,
    body: ICheckCarData
}

export const request = <T>(url: string, params?: IParams): Promise<T>  => {
    return fetch(`https://reacttestprojectapi.azurewebsites.net/${url}`, {
        headers: {
            'X-API-KEY': '3317ec60-d5b1-4d3f-9142-549ee30493ce',
            'Content-Type': 'application/json'
        },
        method: params?.method,
        body: JSON.stringify(params?.body)
    
    }).then(response => response.json())
}
