import { GET_MODELS, GET_MODELS_SUCCESS, GET_MODELS_FAILURE } from '../constants/getModels.constants';
import { IModel } from '../types';

interface GetModelsAction {
    type: typeof GET_MODELS
}
  
interface GetModelsSuccessAction {
    type: typeof GET_MODELS_SUCCESS
    payload: IModel[]
}

interface GetModelsFailureAction {
    type: typeof GET_MODELS_FAILURE
    error: string
}
  
export type ModelsActionTypes = GetModelsAction | GetModelsSuccessAction | GetModelsFailureAction;

export const getModelsAction = (): ModelsActionTypes => ({ type: GET_MODELS });
export const getModelsSuccessAction = (payload: IModel[]): ModelsActionTypes => ({ type: GET_MODELS_SUCCESS, payload });
export const getModelsFailureAction = (error: string): ModelsActionTypes => ({ type: GET_MODELS_FAILURE, error });