import { GET_MODEL, GET_MODEL_SUCCESS, GET_MODEL_FAILURE, CHOOSE_OPTION, SELECT_COLOR, CHECK_CAR, CHECK_CAR_SUCCESS, CHECK_CAR_FAILURE } from '../constants/getModel.constants';
import { IModelConfigurator } from '../types';

interface GetModelAction {
    type: typeof GET_MODEL
}
  
interface GetModelSuccessAction {
    type: typeof GET_MODEL_SUCCESS
    payload: IModelConfigurator
}

interface GetModelFailureAction {
    type: typeof GET_MODEL_FAILURE
    error: string
}

interface ChooseOptionAction {
    type: typeof CHOOSE_OPTION
    payload: number
}

interface SelectColorAction {
    type: typeof SELECT_COLOR,
    payload: number,
    price: number
}

interface CheckCarAction {
    type: typeof CHECK_CAR
}
  
interface CheckCarSuccessAction {
    type: typeof CHECK_CAR_SUCCESS
    payload: any
}

interface CheckCarFailureAction {
    type: typeof CHECK_CAR_FAILURE
    error: string
}
  
export type ModelActionTypes = 
            GetModelAction | 
            GetModelSuccessAction | 
            GetModelFailureAction |
            ChooseOptionAction | 
            SelectColorAction | 
            CheckCarAction | 
            CheckCarSuccessAction | 
            CheckCarFailureAction;

export const getModelAction = (): ModelActionTypes => ({ type: GET_MODEL });
export const getModelSuccessAction = (payload: IModelConfigurator): ModelActionTypes => ({ type: GET_MODEL_SUCCESS, payload });
export const getModelFailureAction = (error: string): ModelActionTypes => ({ type: GET_MODEL_FAILURE, error });

export const chooseOptionAction = (payload: number): ModelActionTypes => ({ type: CHOOSE_OPTION, payload });
export const selectColorAction = (payload: number, price: number): ModelActionTypes => ({ type: SELECT_COLOR, payload, price });

export const checkCarAction = (): ModelActionTypes => ({ type: CHECK_CAR });
export const checkCarSuccessAction = (payload: any): ModelActionTypes => ({ type: CHECK_CAR_SUCCESS, payload });
export const checkCarFailureAction = (error: string): ModelActionTypes => ({ type: CHECK_CAR_FAILURE, error });