import { GET_MODEL, GET_MODEL_SUCCESS, GET_MODEL_FAILURE, CHOOSE_OPTION, SELECT_COLOR, CHECK_CAR, CHECK_CAR_SUCCESS, CHECK_CAR_FAILURE } from '../constants/getModel.constants';
import { ModelActionTypes } from '../actions/getModel.actions';
import { IModelState } from '../types';
import { reusableSort } from '../../shared/helpers';

export const initialState: IModelState = {
    loading: false,
    model: null,
    option: 0,
    color: 0,
    price: 0,
    status: null,
    error: null
}

export const modelReducer = (state = initialState, action: ModelActionTypes): IModelState => {
    switch(action.type) {
        case GET_MODEL:
        case CHECK_CAR:  
            return {
                ...state,
                    loading: true
            }
        case GET_MODEL_SUCCESS: 
            reusableSort(action.payload.trims);
            reusableSort(action.payload.trims[state.option].colors);
            
            return {
                ...state,
                    loading: false,
                    model: action.payload,
                    option: 0,
                    color: 0,
                    price: action.payload.trims[state.option].price
            }
        case CHECK_CAR_SUCCESS:            
            return {
                ...state,
                    loading: false,
                    status: action.payload
            }    
        case GET_MODEL_FAILURE:
        case CHECK_CAR_FAILURE:     
            return {
                ...state,
                    loading: false,
                    error: action.error
            }  
        case CHOOSE_OPTION:
            reusableSort(state.model!.trims[action.payload].colors);
            return {
                ...state,
                    option: action.payload,
                    color: 0,
                    price: state.model!.trims[action.payload].price
            }    
        case SELECT_COLOR:    
            return {
                ...state,
                color: action.payload,
                model: state.model,
                price: state.model!.trims[state.option].price + action.price
            }    
        default:
            return state;     
    }
}