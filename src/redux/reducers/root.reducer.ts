import { combineReducers } from 'redux';
import { modelsReducer } from './models.reducer';
import { modelReducer } from './model.reducer';

export const rootReducer = combineReducers({
    modelsReducer,
    modelReducer
})

export type RootState = ReturnType<typeof rootReducer>;