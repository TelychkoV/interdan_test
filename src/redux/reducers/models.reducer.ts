import { GET_MODELS, GET_MODELS_SUCCESS, GET_MODELS_FAILURE } from '../constants/getModels.constants';
import { ModelsActionTypes } from '../actions/getModels.actions';
import { IModelsState } from '../types';

export const initialState: IModelsState = {
    loading: false,
    models: null,
    error: null
}

export const modelsReducer = (state = initialState, action: ModelsActionTypes): IModelsState => {
    switch(action.type) {
        case GET_MODELS:
            return {
                ...state,
                    loading: true
            }
        case GET_MODELS_SUCCESS:            
            return {
                ...state,
                    loading: false,
                    models: action.payload
            }
        case GET_MODELS_FAILURE:
            return {
                ...state,
                    loading: false,
                    error: action.error
            }   
        default:
            return state;     
    }
}