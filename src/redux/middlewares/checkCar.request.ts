import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../reducers/root.reducer';
import { checkCarAction, checkCarSuccessAction } from '../actions/getModel.actions';
import { ICheckCarData } from '../types';
import { request } from '../../api/request';

export const checkCarRequest = (data: ICheckCarData): ThunkAction<void, RootState, unknown, Action<string>> => 
    dispatch => {
      dispatch(checkCarAction());
  
      return request('Cars/lead', {
          method: 'POST',
          body: data
      })
        .then(() => dispatch(checkCarSuccessAction(true)))
        .catch(error => {
          if(error.message === 'Unexpected end of JSON input') {
            dispatch(checkCarSuccessAction(true));
          } else {
            dispatch(checkCarSuccessAction(false));
          }
        }
    )
}