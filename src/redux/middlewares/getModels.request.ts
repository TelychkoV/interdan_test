import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../reducers/root.reducer';
import { getModelsAction, getModelsSuccessAction, getModelsFailureAction } from '../actions/getModels.actions';
import { request } from '../../api/request';

export const getModelsRequest = (): ThunkAction<void, RootState, unknown, Action<string>> => 
    dispatch => {
      dispatch(getModelsAction());

      return request('Cars/Models')
        .then((data: any) => dispatch(getModelsSuccessAction(data)))
        .catch((error: string ) => dispatch(getModelsFailureAction(error)))
}