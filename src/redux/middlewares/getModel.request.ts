import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../reducers/root.reducer';
import { getModelAction, getModelSuccessAction, getModelFailureAction } from '../actions/getModel.actions';
import { request } from '../../api/request';

export const getModelRequest = (code: string | object | undefined | null): ThunkAction<void, RootState, unknown, Action<string>> => 
    dispatch => {
      dispatch(getModelAction());

      return request(`Cars/Model/${code}`)
        .then((data: any) => dispatch(getModelSuccessAction(data)))
        .catch((error: string) => dispatch(getModelFailureAction(error)))
}