import { useSelector as useReduxSelector, TypedUseSelectorHook } from "react-redux";
import { RootState } from '../redux/reducers/root.reducer';

export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

export interface IModel {
    code: string,
    name: string,
    priceFrom: string,
    imageUrl: string
}

export interface IModelsState {
    loading: boolean,
    models?: null | IModel[],
    error?: null | string
}

export interface IModelState {
    loading: boolean,
    model?: null | IModelConfigurator | any,
    option: number,
    color: number,
    price: number,
    status: null | boolean,
    statusError?: null | string,
    error?: null | string
}

export interface ICheckCarData {
    modelName: string,
    trimName: string,
    colorName: string
}

export interface IColor {
    name: string,
    iconUrl: string,
    imageUrl: string,
    price: number
}

export interface ITrim {
    name: string,
    price: number,
    colors: IColor[]
}

export interface IModelConfigurator {
    code: string,
    name: string,
    trims: ITrim[]
}

export type IColorOrTrim = IColor | ITrim;