import React, { StrictMode } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store.config';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import './styles/index.scss';

render(
  <Provider store={store}>
     <StrictMode>
        <App />
     </StrictMode>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
